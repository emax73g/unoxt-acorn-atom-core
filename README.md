# UnoXT Acorn Atom Core

This is port of ZXUno Acorn Atom Core 

https://github.com/zxdos/zxuno/tree/master/cores/AcornAtom

Keyboard:

SHIFT+10 - Menu

F10 - Reset

Turbo: F1 = 1Mhz, F2 = 2Mhz, F3 = 4Mhz, F4 = 8Mhz

SD Card FAT32 https://github.com/hoglet67/AtomSoftwareArchive/releases

